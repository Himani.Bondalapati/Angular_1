import { Component } from '@angular/core';
import { User } from './signupinterface';
@Component({
  selector: 'app-signupform',
  templateUrl: './signupform.component.html',
  styleUrls: ['./signupform.component.css']
})
export class SignupformComponent {
  user: User = {
    name: '',
    account: {
      email: '',
      confirm: ''
    }
  };
}
