import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
export class ToDoItem {
  readonly id: number;
  readonly label: string;
  readonly complete: boolean;
}
@Injectable()

export class TodoService {
  todos: ToDoItem[] = [{
    label: 'Eat pizza',
    id: 0,
    complete: true
  }, {
    label: 'Do some coding',
    id: 1,
    complete: true
  }, {
    label: 'Sleep',
    id: 2,
    complete: false
  }, {
    label: 'Print tickets',
    id: 3,
    complete: true
  }];

 getTodos() {
   return this.todos;
}
}
