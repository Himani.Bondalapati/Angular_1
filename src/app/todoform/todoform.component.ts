import { Component, Output, EventEmitter, Input} from '@angular/core';


@Component({
  selector: 'app-todoform',
  templateUrl: './todoform.component.html',
  styleUrls: ['./todoform.component.css']
})
export class TodoformComponent {
  label: string;
  @Input() selectedToDo: any;
  @Input() isEdit: boolean;
  @Input() todo: any;
  @Output() onAdd = new EventEmitter();
  @Output() update = new EventEmitter();
  @Output() onEditClick = new EventEmitter();


  submit() {
    if (!this.label) {
      return;
    }
    this.onAdd.emit({label: this.label});
    this.label = '';
  }
}
