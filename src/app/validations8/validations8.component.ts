import { Component } from '@angular/core';
import { User } from '../signupinterface';

@Component({
  selector: 'app-validations8',
  templateUrl: './validations8.component.html',
})
export class Validations8Component {
  user: User = {
    name: '',
    account: {
      email: '',
      confirm: ''
    }
  };
  onSubmit({ value, valid }: { value: User, valid: boolean }) {
    console.log(value, valid);
  }
}



