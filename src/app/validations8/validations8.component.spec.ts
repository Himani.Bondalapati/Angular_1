import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Validations8Component } from './validations8.component';

describe('Validations8Component', () => {
  let component: Validations8Component;
  let fixture: ComponentFixture<Validations8Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Validations8Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Validations8Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
