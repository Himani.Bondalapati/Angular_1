import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TestComponent } from './test/test.component';
import { CounterComponent } from './counter/counter.component';
import { Component, Input} from '@angular/core';
import { TodoformComponent } from './todoform/todoform.component';
import { TodoListComponent } from './todo-list/todo-list.component';
import { TodoComponent } from './todo/todo.component';
import { TodosComponent } from './todos/todos.component';
import { TodoService } from './todo.service';
import { FormsModule } from '@angular/forms';
import { SignupformComponent } from './signupform/signupform.component';
import { Validations8Component } from './validations8/validations8.component';
@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    CounterComponent,
    TodoformComponent,
    TodoListComponent,
    TodoComponent,
    TodosComponent,
    SignupformComponent,
    Validations8Component
  ],
  imports: [ BrowserModule , FormsModule
  ],
  providers: [ TodoService ],
  bootstrap: [AppComponent],
})
export class AppModule { }
