import { Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.css']
})
export class CounterComponent  {
  @Input('init') count: number;
  @Output('update')
  Onchange: EventEmitter<number>= new EventEmitter<number>();
  increment() {
    this.count++;
    this.Onchange.emit(this.count);
  }
  decrement() {
    this.count--;
    this.Onchange.emit(this.count);
  }
}
